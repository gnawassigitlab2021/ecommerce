from django.db import models

# Create your models here.
class Produit(models.Model):
    nom = models.CharField(max_length=200, null=True )
    descrption = models.CharField(max_length=500, null=True)
    prix_achat = models.FloatField(null=True)
    remise = models.FloatField(null=True)
    prix_livraison = models.FloatField(null=True)
    marque = models.CharField(max_length=200, null=True )
    image = models.CharField(max_length=200, null=True )
    date_creation = models.DateTimeField(auto_now_add=True, null= True)